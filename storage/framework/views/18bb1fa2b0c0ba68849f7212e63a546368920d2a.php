<?php $request = app('Illuminate\Http\Request'); ?>
<div class="row">
	<div class="col-xs-12 text-center">
		<table class=""  cellpadiing="0" style="font-size:11px; width: 100%;text-align:left;">
			<tr><?php
				// dd($receipt_details->logo);
				$logo = $receipt_details->logo;
				$path = Storage::url('invoice_logos/'. $logo);
				// dd($path);
			?>
			
				<td rowspan="6"  style="padding-right:8px; width: 100px; "><img style="width: 100%;" src="<?php echo e(url($path)); ?>"></td>
				<td style="padding-top:35px; font-size:16px; font-weight: bold "><?php echo $receipt_details->display_name; ?></td>
				<td style="align-content: right; width:30%; font-size:16px; font-weight: bold ">FAKTUR PENJUALAN</td>
				
			</tr>
			<tr>
				<td style=""><?php echo e($receipt_details->address); ?></td>
				<td style="align-content: right; width:30%;"><?php echo e($receipt_details->kota); ?>, <?php echo e($receipt_details->tgl_trans); ?></td>
			</tr>
			<tr>
				<td><?php echo e($receipt_details->contact); ?></td>
				<td style="align-content: right; width:30%;">Kepada :</td>
			</tr>
			<tr>
				<td></td>
				<td style="align-content:right; font-weight: bold "><?php echo e(strtoupper($receipt_details->customer_name)); ?></td>
			</tr>
			<tr>
				
				<td></td>
				<td style="align-content: right;"><?php echo e($receipt_details->customer_almt1); ?></td>
			</tr>
			<tr>
				<td></td>
				<td style="align-content: right;"><?php echo e($receipt_details->customer_almt2); ?> Hp.(<?php echo e($receipt_details->customer_almt3); ?>)</td>
			</tr>
		</table>
	</div>
	<div class="col-xs-12">
		<table class="" cellpadiing="0" style="margin-top:10px;margin-bottom:10px;font-size:11px; width: 100%;font-size:11px;text-align:left;">	
		<tr style="border-top:0.05px dashed black;">
			
			<td style=" padding-top:7px; padding-left:10px; font-size:11px; padding-bottom:0px;">No : <?php echo $receipt_details->invoice_no; ?></td>
			<td style="padding-top:7px; padding-left:10px; font-size:11px; padding-bottom:0px;">Type Bayar : <?php echo $receipt_details->tipe_byr; ?></td>
			<td style="padding-top:7px; padding-left:10px; font-size:11px; padding-bottom:0px;">Jatuh Tempo : ................</td>
		</tr>
			
				<table class="" cellpading="0" style="border:0.01px solid black; border-collapse: collapse;font-size:11px; width: 100%;font-size:11px;text-align:left;">
					<thead  style="border:0.05px solid black;">
						<tr>
							<th style="padding: 3px; border-right:0.05px solid black; text-align:center; width:5%">No</th>
							<th style="padding: 3px; border-right:0.05px solid black; text-align:center;">Nama Barang</th>
							<th style="padding: 3px; border-right:0.05px solid black; text-align:center;width:10%">Satuan</th>
							<th style="padding: 3px; border-right:0.05px solid black; text-align:center;width:11%">Harga Satuan</th>
							<th style="padding: 3px; border-right:0.05px solid black; text-align:center; width:5%">JML</th>
							<th style="padding: 3px; border-right:0.05px solid black; text-align:center; width:10%">Diskon</th>
							<th style="padding: 3px; border-right:0.05px solid black; text-align:center;">Total Harga</th>
						</tr>
					</thead>
					<?php $__empty_1 = true; $__currentLoopData = $receipt_details->lines; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $line): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
							<tr>
								<td style="padding: 3px; border-right:0.05px solid black;text-align:center;"><?php echo e($loop->iteration); ?> </td>
								<td style="padding: 3px; border-right:0.05px solid black;"><?php echo e($line['name']); ?> <?php echo e($line['variation']); ?></td>
								<td style="padding: 3px; border-right:0.05px solid black; text-align:center;"><?php echo e($line['units']); ?></td>
								<td style="padding: 3px; border-right:0.05px solid black;text-align:right;"><?php echo e(number_format(round($line['unit_price_inc_tax']))); ?></td>
								<td style="padding: 3px; border-right:0.05px solid black;text-align:center;"><?php echo e($line['quantity']); ?> </td>
								<td style="padding: 3px; border-right:0.05px solid black;text-align:center;"> </td>
								<td style="padding: 3px; border-right:0.05px solid black;text-align:right;"><?php echo e(number_format($line['line_total'])); ?></td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						
				</table>
			</table>
			<table cellpading="0" style="border-left:0.01px solid black; border-right:0.01px solid black; border-bottom:0.01px solid black; border-collapse: collapse; font-size:11px; width: 100%; font-size:11px; text-align:right;">
				<tr>
					<td style=""><b>Sub Total : &nbsp;&nbsp;Rp.</td>
					
					<td style="padding: 3px;">
					<?php echo e(number_format(round($receipt_details->subtotal_unformatted))); ?></td>
				</tr>
				<tr>
					<td style="width: 600px;"><b>Diskon : &nbsp;&nbsp;Rp.</td>
					
					<td style="padding: 3px;">
					<?php echo e(number_format(round($receipt_details->discount))); ?></td>
				</tr>
				<tr>
					<td style=""><b>Total : &nbsp;&nbsp;Rp.</td>
					
					<td style="padding: 3px;">
					<?php echo e(number_format(round($receipt_details->total))); ?></td>
				</tr>
			</table>
		<br>
		<table class="" style="font-size:11px; width: 100%;font-size:11px;text-align:center;">
			<thead>
				<th style="text-align:center;">Penerima</th>
				<th style="text-align:center;">Sales/Pengirim</th>
				<th style="text-align:center;">Bag. Gudang</th>
				<th style="text-align:center;">Admin/Kasir</th>
			</thead>
			<tr><td><br><br><br></td></tr>
			<tr>
				
				<td style="align-content:right; font-weight: bold ">(<?php echo e(strtoupper($receipt_details->customer_name)); ?>)</td>
				<td>(....................)</td>
				<td>(....................)</td>
				
				<td style="align-content:right; font-weight: bold ">(<?php echo e(auth()->user()->first_name." ".auth()->user()->last_name); ?>)</td>
			</tr>
		</table>
	</div>
</div>