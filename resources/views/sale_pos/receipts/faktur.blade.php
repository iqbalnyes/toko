@inject('request', 'Illuminate\Http\Request')
<div class="row">
	<div class="col-xs-12 text-center">
		<table class=""  cellpadiing="0" style="font-size:11px; width: 100%;text-align:left;">
			<tr>@php
				// dd($receipt_details->logo);
				$logo = $receipt_details->logo;
				$path = Storage::url('invoice_logos/'. $logo);
				// dd($path);
			@endphp
			{{-- <h2 class="text-center"><img style="width: 10%;" src="{{url($path)}}"></h2> --}}
				<td rowspan="6"  style="padding-right:8px; width: 100px; "><img style="width: 100%;" src="{{url($path)}}"></td>
				<td style="padding-top:35px; font-size:16px; font-weight: bold ">{!! $receipt_details->display_name !!}</td>
				<td style="align-content: right; width:30%; font-size:16px; font-weight: bold ">FAKTUR PENJUALAN</td>
				
			</tr>
			<tr>
				<td style="">{{$receipt_details->address}}</td>
				<td style="align-content: right; width:30%;">{{$receipt_details->kota}}, {{$receipt_details->tgl_trans}}</td>
			</tr>
			<tr>
				<td>{{$receipt_details->contact}}</td>
				<td style="align-content: right; width:30%;">Kepada :</td>
			</tr>
			<tr>
				<td></td>
				<td style="align-content:right; font-weight: bold ">{{strtoupper($receipt_details->customer_name)}}</td>
			</tr>
			<tr>
				{{-- <td style="text-align:right; ">{{$receipt_details->customer_name}}</td> --}}
				<td></td>
				<td style="align-content: right;">{{$receipt_details->customer_almt1}}</td>
			</tr>
			<tr>
				<td></td>
				<td style="align-content: right;">{{$receipt_details->customer_almt2}} Hp.({{$receipt_details->customer_almt3}})</td>
			</tr>
		</table>
	</div>
	<div class="col-xs-12">
		<table class="" cellpadiing="0" style="margin-top:10px;margin-bottom:10px;font-size:11px; width: 100%;font-size:11px;text-align:left;">	
		<tr style="border-top:0.05px dashed black;">
			{{-- <hr style="color: black"> --}}
			<td style=" padding-top:7px; padding-left:10px; font-size:11px; padding-bottom:0px;">No : {!! $receipt_details->invoice_no !!}</td>
			<td style="padding-top:7px; padding-left:10px; font-size:11px; padding-bottom:0px;">Type Bayar : {!! $receipt_details->tipe_byr !!}</td>
			<td style="padding-top:7px; padding-left:10px; font-size:11px; padding-bottom:0px;">Jatuh Tempo : ................</td>
		</tr>
			
				<table class="" cellpading="0" style="border:0.01px solid black; border-collapse: collapse;font-size:11px; width: 100%;font-size:11px;text-align:left;">
					<thead  style="border:0.05px solid black;">
						<tr>
							<th style="padding: 3px; border-right:0.05px solid black; text-align:center; width:5%">No</th>
							<th style="padding: 3px; border-right:0.05px solid black; text-align:center;">Nama Barang</th>
							<th style="padding: 3px; border-right:0.05px solid black; text-align:center;width:10%">Satuan</th>
							<th style="padding: 3px; border-right:0.05px solid black; text-align:center;width:11%">Harga Satuan</th>
							<th style="padding: 3px; border-right:0.05px solid black; text-align:center; width:5%">JML</th>
							<th style="padding: 3px; border-right:0.05px solid black; text-align:center; width:10%">Diskon</th>
							<th style="padding: 3px; border-right:0.05px solid black; text-align:center;">Total Harga</th>
						</tr>
					</thead>
					@forelse($receipt_details->lines as $line)
							<tr>
								<td style="padding: 3px; border-right:0.05px solid black;text-align:center;">{{ $loop->iteration }} </td>
								<td style="padding: 3px; border-right:0.05px solid black;">{{$line['name']}} {{$line['variation']}}</td>
								<td style="padding: 3px; border-right:0.05px solid black; text-align:center;">{{$line['units']}}</td>
								<td style="padding: 3px; border-right:0.05px solid black;text-align:right;">{{number_format(round($line['unit_price_inc_tax']))}}</td>
								<td style="padding: 3px; border-right:0.05px solid black;text-align:center;">{{$line['quantity']}} </td>
								<td style="padding: 3px; border-right:0.05px solid black;text-align:center;"> </td>
								<td style="padding: 3px; border-right:0.05px solid black;text-align:right;">{{number_format($line['line_total'])}}</td>
							</tr>
						@endforeach
						
				</table>
			</table>
			<table cellpading="0" style="border-left:0.01px solid black; border-right:0.01px solid black; border-bottom:0.01px solid black; border-collapse: collapse; font-size:11px; width: 100%; font-size:11px; text-align:right;">
				<tr>
					<td style=""><b>Sub Total : &nbsp;&nbsp;Rp.</td>
					{{-- <td style="width: 0%;">Rp.</td> --}}
					<td style="padding: 3px;">
					{{number_format(round($receipt_details->subtotal_unformatted))}}</td>
				</tr>
				<tr>
					<td style="width: 600px;"><b>Diskon : &nbsp;&nbsp;Rp.</td>
					{{-- <td style="padding-left: 3px;"></td> --}}
					<td style="padding: 3px;">
					{{number_format(round($receipt_details->discount))}}</td>
				</tr>
				<tr>
					<td style=""><b>Total : &nbsp;&nbsp;Rp.</td>
					{{-- <td style="padding-left: 3px;">Rp.</td> --}}
					<td style="padding: 3px;">
					{{number_format(round($receipt_details->total))}}</td>
				</tr>
			</table>
		<br>
		<table class="" style="font-size:11px; width: 100%;font-size:11px;text-align:center;">
			<thead>
				<th style="text-align:center;">Penerima</th>
				<th style="text-align:center;">Sales/Pengirim</th>
				<th style="text-align:center;">Bag. Gudang</th>
				<th style="text-align:center;">Admin/Kasir</th>
			</thead>
			<tr><td><br><br><br></td></tr>
			<tr>
				{{-- <td>()</td> --}}
				<td style="align-content:right; font-weight: bold ">({{strtoupper($receipt_details->customer_name)}})</td>
				<td>(....................)</td>
				<td>(....................)</td>
				{{-- <td>(....................)</td> --}}
				<td style="align-content:right; font-weight: bold ">({{auth()->user()->first_name." ".auth()->user()->last_name}})</td>
			</tr>
		</table>
	</div>
</div>